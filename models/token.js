var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tokenSchema = new Schema ({
_userId:{type: mongoose.Schema.Types.ObjectId, ref:'Usuario'},
token:{type: String, default:false},
createdAt:{type:Date, required:true, default:Date.now, expires: 43200 }
});

module.exports=mongoose.model('Token', tokenSchema);