const express= require('express');
const router= express.Router();
const authController= require('../../controllers/api/authControllers');
const passport = require('passport');


router.post('/authenticate', authController.aunthenticate);
router.post('/forgotPassword', authController.forgotPassword);
router.post('/facebook_token',passport.authenticate('facebook-token'),authController.authFacebookToken);
module.exports= router;