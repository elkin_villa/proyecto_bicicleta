var express=require('express');
var route=express.Router();
var usuarioController=require('../../controllers/api/usuarioControllerApi');
const router = require('..');

router.get('/',usuarioController.usuarios_list);
router.post('/create',usuarioController.usuarios_create);
router.post('/reserva',usuarioController.usuarios_reservar)

module.exports=router;